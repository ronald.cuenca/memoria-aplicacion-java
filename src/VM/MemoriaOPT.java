/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package VM;

/**
 *
 * @author LENOVO
 */
public class MemoriaOPT {
    public final static int OVERFLOW = 200000000;
    private String salida;
    private int tamanio;

    public MemoriaOPT(int lenght) {
        this.tamanio = lenght;
        int i = 0;
        while (i < lenght) {
            i++;
            try {         
                this.salida += "a";
            } catch (OutOfMemoryError e) {
                break;
            }
        }
    }

    public String getOom() {
        return salida;
    }

    public int getLength() {
        return tamanio;
    }

    public static void main(String[] args) {
        MemoriaOPT javaHeapTest = new MemoriaOPT(OVERFLOW);
        System.out.println(javaHeapTest.getLength());
    }
}
